#define _CRT_SECURE_NO_WARNINGS 
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
int main()
{
	int leftlimit, rightlimit, num;
	printf("Enter the interval, example 1-9:");
	scanf("%d-%d", &leftlimit, &rightlimit);
	printf("[");
	for (num = leftlimit; num < rightlimit; num++)
		printf("%d, ", num);
	printf("%d]\n", rightlimit);
}